package org.iborodin.dp.restcrudservice;

import org.iborodin.dp.restcrudservice.entity.Project;
import org.iborodin.dp.restcrudservice.service.ProjectService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectServiceTests {

    @Autowired
    private ProjectService projectService;

    @Before
    public void clearProjects() {
        projectService.deleteAll();
    }


    @Test
    public void testCreateProject() {
        Assert.assertTrue(projectService.findAll().isEmpty());

        final Project project = new Project();
        project.setName("test");

        projectService.save(project);

        Assert.assertEquals(1, projectService.findAll().size());
        Assert.assertEquals("test", projectService.findAll().get(0).getName());
    }
}
