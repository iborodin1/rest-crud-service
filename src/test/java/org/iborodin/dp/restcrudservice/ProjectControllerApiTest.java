/*
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.iborodin.dp.restcrudservice;

import io.swagger.client.api.ProjectControllerApi;
import io.swagger.client.model.Project;
import org.iborodin.dp.restcrudservice.RestCrudServiceApplication;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * API tests for ProjectControllerApi
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = RestCrudServiceApplication.class)
public class ProjectControllerApiTest {

    @LocalServerPort
    Integer randomServerPort;

    private final ProjectControllerApi api = new ProjectControllerApi();

    @Before
    public void setupPort() {
        api.getApiClient().setBasePath("http://localhost:" + randomServerPort.toString());
        api.deleteProjectsUsingDELETE();
    }

    @Test
    public void allUsingGETTest() {
        List<Project> response = api.allUsingGET();

        Assert.assertTrue(response.isEmpty());
    }

    @Test
    public void deleteProjectUsingDELETETest() {
        Project newProject = new Project();
        newProject.setName("test");
        Project response = api.newProjectUsingPOST(newProject);

        Assert.assertFalse(api.allUsingGET().isEmpty());

        Long id = response.getId();
        api.deleteProjectUsingDELETE(id);

        Assert.assertTrue(api.allUsingGET().isEmpty());
    }

    @Test
    public void deleteProjectsUsingDELETETest() {
        api.deleteProjectsUsingDELETE();

        Assert.assertTrue(api.allUsingGET().isEmpty());
    }

    @Test
    public void newProjectUsingPOSTTest() {
        Project newProject = new Project();
        newProject.setName("test");
        Project response = api.newProjectUsingPOST(newProject);

        Assert.assertFalse(api.allUsingGET().isEmpty());
    }

    @Test
    public void newProjectsUsingPOSTTest() {
        List<Project> newProjects = new ArrayList<>();
        Project newProject = new Project();
        newProject.setName("test");
        newProjects.add(newProject);

        List<Project> response = api.newProjectsUsingPOST(newProjects);

        Assert.assertFalse(api.allUsingGET().isEmpty());
    }

    @Test
    public void oneUsingGETTest() {
        Project newProject = new Project();
        newProject.setName("test");
        Project createdProject = api.newProjectUsingPOST(newProject);

        Long id = createdProject.getId();
        Project response = api.oneUsingGET(id);

        Assert.assertEquals("test", response.getName());
    }

    @Test
    public void replaceProjectUsingPUTTest() {
        Project newProject = new Project();
        newProject.setName("test");
        Project createdProject = api.newProjectUsingPOST(newProject);

        createdProject.setDescription("dadada");

        Project response = api.replaceProjectUsingPUT(createdProject);

        Project updatedProject = api.oneUsingGET(createdProject.getId());

        Assert.assertEquals("dadada", updatedProject.getDescription());
    }

    @Test
    public void replaceProjectsUsingPUTTest() {
        Project newProject = new Project();
        newProject.setName("test");
        Project createdProject = api.newProjectUsingPOST(newProject);

        createdProject.setDescription("dadada");

        List<Project> newProjects = new ArrayList<>();
        newProjects.add(createdProject);

        List<Project> response = api.replaceProjectsUsingPUT(newProjects);

        Project updatedProject = api.oneUsingGET(createdProject.getId());

        Assert.assertEquals("dadada", updatedProject.getDescription());
    }

}
