package org.iborodin.dp.restcrudservice.repository;

import org.iborodin.dp.restcrudservice.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProjectRepository extends JpaRepository<Project, Long> {
}
