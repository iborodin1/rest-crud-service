package org.iborodin.dp.restcrudservice.repository;

import org.iborodin.dp.restcrudservice.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITaskRepository extends JpaRepository<Task, Long> {
}
