package org.iborodin.dp.restcrudservice.service;

import org.iborodin.dp.restcrudservice.entity.Project;
import org.iborodin.dp.restcrudservice.repository.IProjectRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    private final IProjectRepository ProjectRepository;

    public ProjectService(final IProjectRepository ProjectRepository) {
        this.ProjectRepository = ProjectRepository;
    }

    public List<Project> findAll() {
        return ProjectRepository.findAll();
    }

    public Project getById(Long id) {
        return ProjectRepository.getById(id);
    }

    public Project save(Project entity) {
        return ProjectRepository.save(entity);
    }

    public void deleteById(Long id) {
        ProjectRepository.deleteById(id);
    }

    public List<Project> saveAll(Iterable<Project> entities) {
        return ProjectRepository.saveAll(entities);
    }

    public void deleteAll() {
        ProjectRepository.deleteAll();
    }
}