package org.iborodin.dp.restcrudservice.service;

import org.iborodin.dp.restcrudservice.entity.Task;
import org.iborodin.dp.restcrudservice.repository.ITaskRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task getById(Long id) {
        return taskRepository.getById(id);
    }

    public Task save(Task entity) {
        return taskRepository.save(entity);
    }

    public void deleteById(Long id) {
        taskRepository.deleteById(id);
    }

    public List<Task> saveAll(Iterable<Task> entities) {
        return taskRepository.saveAll(entities);
    }

    public void deleteAll() {
        taskRepository.deleteAll();
    }
}
