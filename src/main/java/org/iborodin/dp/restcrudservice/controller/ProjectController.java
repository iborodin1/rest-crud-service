package org.iborodin.dp.restcrudservice.controller;

import org.iborodin.dp.restcrudservice.entity.Project;
import org.iborodin.dp.restcrudservice.service.ProjectService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/")
public class ProjectController {

    private final ProjectService ProjectService;

    public ProjectController(final ProjectService ProjectService) {
        this.ProjectService = ProjectService;
    }

    @GetMapping("/projects")
    List<Project> all() {
        return ProjectService.findAll();
    }

    @GetMapping("/project/{id}")
    Project one(@PathVariable Long id) {
        return ProjectService.getById(id);
    }

    @PostMapping("/project")
    Project newProject(@RequestBody Project newProject) {
        return ProjectService.save(newProject);
    }

    @PutMapping("/project")
    Project replaceProject(@RequestBody Project newProject) {
        return ProjectService.save(newProject);
    }

    @DeleteMapping("/project/{id}")
    void deleteProject(@PathVariable Long id) {
        ProjectService.deleteById(id);
    }

    @PostMapping("/projects")
    List<Project> newProjects(@RequestBody List<Project> newProjects) {
        return ProjectService.saveAll(newProjects);
    }

    @PutMapping("/projects")
    List<Project> replaceProjects(@RequestBody List<Project> newProjects) {
        return ProjectService.saveAll(newProjects);
    }

    @DeleteMapping("/projects")
    void deleteProjects() {
        ProjectService.deleteAll();
    }
}