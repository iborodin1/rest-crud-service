package org.iborodin.dp.restcrudservice.controller;

import org.iborodin.dp.restcrudservice.entity.Task;
import org.iborodin.dp.restcrudservice.service.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {

    private final TaskService taskService;

    public TaskController(final TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    List<Task> all() {
        return taskService.findAll();
    }

    @GetMapping("/task/{id}")
    Task one(@PathVariable Long id) {
        return taskService.getById(id);
    }

    @PostMapping("/task")
    Task newTask(@RequestBody Task newTask) {
        return taskService.save(newTask);
    }

    @PutMapping("/task")
    Task replaceTask(@RequestBody Task newTask) {
        return taskService.save(newTask);
    }

    @DeleteMapping("/task/{id}")
    void deleteTask(@PathVariable Long id) {
        taskService.deleteById(id);
    }

    @PostMapping("/tasks")
    List<Task> newTasks(@RequestBody List<Task> newTasks) {
        return taskService.saveAll(newTasks);
    }

    @PutMapping("/tasks")
    List<Task> replaceTasks(@RequestBody List<Task> newTasks) {
        return taskService.saveAll(newTasks);
    }

    @DeleteMapping("/tasks")
    void deleteTasks() {
        taskService.deleteAll();
    }
}
